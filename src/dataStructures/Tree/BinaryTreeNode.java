package dataStructures.Tree;

import java.util.Stack;

public class BinaryTreeNode {
    public int data;
    public BinaryTreeNode left;
    public BinaryTreeNode right;

    public BinaryTreeNode(int data) {
        this.data = data;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public BinaryTreeNode getLeft() {
        return left;
    }

    public void setLeft(BinaryTreeNode left) {
        this.left = left;
    }

    public BinaryTreeNode getRight() {
        return right;
    }

    public void setRight(BinaryTreeNode right) {
        this.right = right;
    }

    public void preOrderRecursive(BinaryTreeNode root) {
        if (root != null) {
            System.out.println(root.data);
            preOrderRecursive(root.left);
            preOrderRecursive(root.right);
        }
    }

    public void preOrderIterative(BinaryTreeNode root) {
        if (root != null) {
            Stack<BinaryTreeNode> stack = new Stack<>();
            stack.push(root);
            while (!stack.empty()) {
                BinaryTreeNode node = stack.pop();
                System.out.println(node.data);
                if (node.right != null) {
                    stack.push(node.right);
                }
                if (node.left != null) {
                    stack.push(node.left);
                }
            }

        }
    }

    public void inOrderRecursive(BinaryTreeNode root) {
        if (root != null) {
            inOrderRecursive(root.left);
            System.out.println(root.data);
            inOrderRecursive(root.right);
        }
    }

    public void inOrderIterative(BinaryTreeNode root) {
        if (root != null) {
            Stack<BinaryTreeNode> stack = new Stack<>();
            BinaryTreeNode currentNode = root;
            boolean done = false;
            while (!done) {
                if (currentNode != null) {
                    stack.push(currentNode);
                    currentNode = currentNode.left;
                } else {
                    if (stack.empty()) {
                        done = true;
                    } else {
                        currentNode = stack.pop();
                        System.out.println(currentNode.data);
                        currentNode = currentNode.right;
                    }
                }
            }
        }
    }

    public void postOrderRecursive(BinaryTreeNode root) {
        if (root != null) {
            postOrderRecursive(root.left);
            postOrderRecursive(root.right);
            System.out.println(root.data);
        }
    }

    public void postOrderIterative(BinaryTreeNode root) {
        if (root != null) {
            Stack<BinaryTreeNode> stack = new Stack<>();
            stack.push(root);
            BinaryTreeNode prev = null;
            while (!stack.empty()) {
                BinaryTreeNode curr = stack.peek();
                if (prev == null || prev.left == curr || prev.right == curr) {
                    if (curr.left != null) {
                        stack.push(curr.left);
                    } else if (curr.right != null) {
                        stack.push(curr.right);
                    } else {
                        System.out.println(curr.data);
                        stack.pop();
                    }
                } else if (curr.left == prev) {
                    if (curr.right != null) {
                        stack.push(curr.right);
                    } else {
                        System.out.println(curr.data);
                        stack.pop();
                    }
                } else {
                    System.out.println(curr.data);
                    stack.pop();
                }
                prev = curr;
            }
        }
    }

}
